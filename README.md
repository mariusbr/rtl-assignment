# rtl-assignment

This application will start a background HostedService that will start scraping the TvMaze api for all show information. 
It will do so by getting the list of when shows where last updated from the API and checking this data against a local
copy. If a show needs to be updated the information is retrieved from the API and saved in the database. 

Is also provides an endpoint at `/shows` that returns the shows with the cast members, sorted descending by birthday.
It takes two prameters for pagination `page`, the page to retrieve, and `take` the number of shows to retrieve eg: `/shows?page=20&take=250`.

To test, build the application like so: `docker build -t tvmaze:latest .`

Than run it like so: `docker run --rm -p 5000:5000 tvmaze`