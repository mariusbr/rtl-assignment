FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build

WORKDIR /sln

COPY ./*.sln ./
COPY src/*/*.csproj ./
RUN for file in $(ls *.csproj); do mkdir -p src/${file%.*}/ && mv $file src/${file%.*}/; done

RUN dotnet restore

COPY ./src ./src
RUN dotnet publish -c Release -o /sln/artifacts --no-restore

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
ENV ASPNETCORE_URLS=http://+:5000
WORKDIR /app
COPY --from=build /sln/artifacts ./

RUN adduser --disabled-password -gecos "" tvmaze --home /home/tvmaze -uid 5000 && chown -R tvmaze /app && mkdir /data && chown -R tvmaze /data

USER tvmaze
EXPOSE 5000
VOLUME ["/data"]
ENTRYPOINT ["dotnet", "TvMazeApi.dll"]