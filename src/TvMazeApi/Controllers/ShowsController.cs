﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TvMazeApi.Data;
using TvMazeApi.Models;

namespace TvMazeApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShowsController : ControllerBase
    {
        private readonly ILogger<ShowsController> _logger;
        private readonly TvMazeDbContext _tvMazeDbContext;

        public ShowsController(ILogger<ShowsController> logger, TvMazeDbContext tvMazeDbContext)
        {
            _logger = logger;
            _tvMazeDbContext = tvMazeDbContext;
        }

        [HttpGet]
        public async Task<IEnumerable<Show>> Get(int page = 1, int take = 200, CancellationToken cancellationToken = default)
        {
            var data = _tvMazeDbContext
                    .Shows
                    .OrderBy(x => x.Id)
                    .Skip((page - 1) * take)
                    .Take(take);
            
            await data.ForEachAsync(x => x.Cast = x.Cast.OrderByDescending(c => c.Birthday).ToList(), cancellationToken);

            return data;
        }
    }
}
