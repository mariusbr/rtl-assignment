﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TvMazeApi.Data;
using TvMazeApi.Models;
using TvMazeApiClient;

namespace TvMazeApi
{
    public interface IShowUpdater
    {
        Task Update(int showId, long updated, CancellationToken cancellationToken = default);
    }

    public class ShowUpdater : IShowUpdater
    {
        private readonly ILogger<ShowUpdater> _logger;
        private readonly TvMazeDbContext _tvMazeDbContext;
        private readonly ITvMazeApiClient _tvMazeApiClient;

        public ShowUpdater(ILogger<ShowUpdater> logger, TvMazeDbContext tvMazeDbContext, ITvMazeApiClient tvMazeApiClient)
        {
            _logger = logger;
            _tvMazeDbContext = tvMazeDbContext;
            _tvMazeApiClient = tvMazeApiClient;
        }

        public async Task Update(int showId, long updated, CancellationToken cancellationToken = default)
        {
            _logger.LogInformation($"Checking for updates for show {showId} with updated value: {updated}");

            var dbShow = await _tvMazeDbContext.Shows.Where(x => x.Id == showId).SingleOrDefaultAsync();
            
            var needsUpdating = dbShow == null || dbShow.Updated < updated;
            if (needsUpdating == false)
            {
                return;
            }

            var show = await _tvMazeApiClient.GetShow(showId, true, cancellationToken);

            _logger.LogDebug($"Updating {showId}:{show.Name}");

            var update = true;

            if (dbShow == null)
            {
                update = false;
                dbShow = new Show()
                {
                    Id = show.Id
                };
            }

            dbShow.Updated = show.Updated;
            dbShow.Name = show.Name;
            dbShow.Cast = show.Embedded?.Cast?
                            .GroupBy(c => c.Person.Id)
                            .Select(g =>
                            {
                                var person = g.First().Person;
                                return new Person() { Id = person.Id, Name = person.Name, Birthday = person.Birthday };
                            })
                            .ToList();

            _tvMazeDbContext.Entry(dbShow).State = update ? EntityState.Modified : EntityState.Added;

            await _tvMazeDbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
