﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TvMazeApiClient;
using TvMazeApiClient.Models;

namespace TvMazeApi
{
    public class TvMazeScraperService : IHostedService
    {
        private const int DelayInMinutes = 5;

        private readonly CancellationTokenSource _shutdown = new CancellationTokenSource();
        private Task _backgroundTask;

        private readonly ILogger<TvMazeScraperService> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ITvMazeApiClient _tvMazeApiClient;

        public TvMazeScraperService(ILogger<TvMazeScraperService> logger, IServiceScopeFactory scopeFactory, ITvMazeApiClient tvMazeApiClient)
        {
            _logger = logger;
            _scopeFactory = scopeFactory;
            _tvMazeApiClient = tvMazeApiClient;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Scraper Service is starting.");

            _backgroundTask = Task.Run(BackgroundProcessing, cancellationToken);

            return Task.CompletedTask;
        }

        private async Task BackgroundProcessing()
        {
            while (!_shutdown.IsCancellationRequested)
            {
                _logger.LogInformation("Updating shows.");

                using (var scope = _scopeFactory.CreateScope())
                {
                    var showUpdater = scope.ServiceProvider.GetRequiredService<IShowUpdater>();

                    var updateData = await _tvMazeApiClient.GetUpdates(_shutdown.Token);
                    foreach(var item in updateData)
                    {
                        var showId = int.Parse(item.Key);
                        var updated = item.Value;
                        await showUpdater.Update(showId, updated, _shutdown.Token);
                    }
                }

                _logger.LogInformation("All shows updated");

                await Task.Delay(DelayInMinutes * 60_000, _shutdown.Token);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Scraper Service is stopping.");

            _shutdown.Cancel();

            return Task.WhenAny(_backgroundTask, Task.Delay(Timeout.Infinite, cancellationToken));
        }
    }
}
