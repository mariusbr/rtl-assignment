﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TvMazeApi.Models
{
    public class Show
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IList<Person> Cast { get; set; }
        
        [JsonIgnore]
        public long Updated { get; set; }
    }
}
