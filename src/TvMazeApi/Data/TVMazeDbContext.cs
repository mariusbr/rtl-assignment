﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeApi.Models;

namespace TvMazeApi.Data
{
    public class TvMazeDbContext : DbContext
    {
        public DbSet<Show> Shows { get; set; }

        protected TvMazeDbContext()
        {
        }

        public TvMazeDbContext(DbContextOptions<TvMazeDbContext> options) : base (options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ShowEntityTypeConfiguration());
        }
    }

    internal class ShowEntityTypeConfiguration : IEntityTypeConfiguration<Show>
    {
        public void Configure(EntityTypeBuilder<Show> builder)
        {
            builder.Property(e => e.Cast).HasConversion(
                v => JsonConvert.SerializeObject(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                v => JsonConvert.DeserializeObject<IList<Person>>(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
                
        }
    }
}
