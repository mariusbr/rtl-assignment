﻿using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace TvMazeApiClient
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddTvMazeApiClient(this IServiceCollection services)
        {
            var policy = HttpPolicyExtensions
                  .HandleTransientHttpError() 
                  .OrResult(response => (int)response.StatusCode == 429) // handle rate limiting; see http://www.tvmaze.com/api#rate-limiting
                  .WaitAndRetryAsync(new[]
                    {
                        TimeSpan.FromSeconds(3),
                        TimeSpan.FromSeconds(5),
                        TimeSpan.FromSeconds(10)
                    });

            services.AddHttpClient<ITvMazeApiClient, TvMazeApiClient>()
                .AddPolicyHandler(policy);

            return services;
        }
    }
}
