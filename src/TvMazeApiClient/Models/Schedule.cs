﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TvMazeApiClient.Models
{
    public class Schedule
    {

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("days")]
        public IList<string> Days { get; set; }
    }


}
