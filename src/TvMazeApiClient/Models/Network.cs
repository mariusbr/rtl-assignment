﻿using Newtonsoft.Json;

namespace TvMazeApiClient.Models
{
    public class Network
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("country")]
        public Country Country { get; set; }
    }


}
