﻿using Newtonsoft.Json;

namespace TvMazeApiClient.Models
{
    public class Rating
    {

        [JsonProperty("average")]
        public double? Average { get; set; }
    }


}
