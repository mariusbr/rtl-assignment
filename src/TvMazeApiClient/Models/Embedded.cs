﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TvMazeApiClient.Models
{
    public class Embedded
    {

        [JsonProperty("cast")]
        public IList<Cast> Cast { get; set; }
    }
}
