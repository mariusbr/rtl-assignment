﻿using Newtonsoft.Json;

namespace TvMazeApiClient.Models
{
    public class Links
    {

        [JsonProperty("self")]
        public Link Self { get; set; }

        [JsonProperty("previousepisode")]
        public Link Previousepisode { get; set; }
    }


}
