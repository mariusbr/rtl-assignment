﻿using Newtonsoft.Json;

namespace TvMazeApiClient.Models
{
    public class Link
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }


}
