﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TvMazeApiClient.Models;

namespace TvMazeApiClient
{
    public interface ITvMazeApiClient
    {
        Task<IDictionary<string, long>> GetUpdates(CancellationToken cancellationToken = default);
        Task<Show> GetShow(int showId, bool embedCast = false, CancellationToken cancellationToken = default);
    }

    public class TvMazeApiClient : ITvMazeApiClient
    {
        private readonly HttpClient _client;

        public TvMazeApiClient(HttpClient client)
        {
            _client = client;
            _client.BaseAddress = new Uri("http://api.tvmaze.com");
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        private async Task<T> GetData<T>(string endpoint, CancellationToken cancellationToken)
        {
            using (var response = await _client.GetAsync($"/{endpoint}", cancellationToken))
            {
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(content);
            }
        }

        public async Task<IDictionary<string, long>> GetUpdates(CancellationToken cancellationToken = default)
        {
            return await GetData<IDictionary<string, long>>("updates/shows", cancellationToken);
        }

        public async Task<Show> GetShow(int showId, bool embedCast = false, CancellationToken cancellationToken = default)
        {
            var embed = embedCast ? "?embed[]=cast" : string.Empty;

            return await GetData<Show>($"shows/{showId}{embed}", cancellationToken);
        }
    }
}
